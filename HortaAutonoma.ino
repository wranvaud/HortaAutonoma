// include the library code:
#include <LiquidCrystal.h>

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// Atribuindo variaveis e valor dos pinos.
int tempo = 500;
int buttonPin = 10; // Botão de mudo.
int buttonVal = 0;
int mute = 1;
int BUZZER = 8;
int ledRed = 13;
int sensorPin = 0; // Sensor de humidade.
int sensorVal = 0;
int potPin = 9; // Potenciometro (regulador de sensibilidade).
int potVal = 0;
int potMax = 1023;
int lightPin = 2; // Sensor de luminosidade.
int lightVal = 0;

void setup() {
  pinMode(ledRed, OUTPUT);
  pinMode(buttonPin, INPUT);
  pinMode(BUZZER, OUTPUT);
  pinMode(potPin, INPUT);
  pinMode(lightPin, INPUT);

  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
}

void loop() {
  // Sensor de umidade.
  sensorVal = analogRead(sensorPin);
  potVal = analogRead(potPin)/2;
  potMax = 1023 - potVal;

  // Botão mudo.
  buttonVal = digitalRead(buttonPin);
  if (buttonVal == 1) {
    if (mute == 1) {
      mute = 0;
    } else {
      mute = 1;
    }
  }
  if (mute == 1) {
    lcd.setCursor(15, 1);
    lcd.print("M");
  }

  // Sensor de luminosidade.
  lightVal = analogRead(lightPin)/100;
  lcd.setCursor(14, 0);
  lcd.print("L");
  lcd.print(lightVal);

  // Resultados no LCD.
  lcd.setCursor(0, 0);
  if (sensorVal < potVal) {
    digitalWrite(ledRed, HIGH);
    lcd.print("Solo seco!");
    if (!mute) {
      tone(8, 263, 300);
    }
  }
  if (potVal <= sensorVal && sensorVal <= potMax) {
    digitalWrite(ledRed, LOW);
    lcd.print("Solo regado");
  }
  if (potMax < sensorVal) {
    lcd.print("Solo alagado!");
    digitalWrite(ledRed, HIGH);
    if (!mute) {
      tone(8, 523, 300);
    }
  }
  lcd.setCursor(0, 1);
  lcd.print(potVal/10);
  lcd.print("< u:");
  lcd.print(sensorVal/10);
  lcd.print(" <");
  lcd.print(potMax/10);
  // Tempo antes de controlar novamente.
  delay(tempo);
  lcd.clear();
}
